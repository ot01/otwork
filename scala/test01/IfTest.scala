object IfTest{
    def act = {
        println(calc.max(List(1,2,3)))
        // println(calc.m(10))
    }
}

object calc{
    def m(x: Int):Int = x match {
        case 1 => 1
        case _ => 0
    }
    def max(x: List[Int]):Int = x match {
        case head => 1
        case _ => 0
    }
}

object ListTest{
    // def act01(): Array = 1::Nil
     def act02:String = "test1"+"test2"
}
object IfTest01{
    def test01 = 100 + 200
    val x = 300;
    def test02 = if(x > 100) 1 else 0
    def test03 = {
        for(i <- List(1,2,3))
            println(i)
        println("end")
    }
    def test04 = {
        for(i <- List(1,2,3);j <- List(4,5))
            println(i + ":" + j)
    }
}

object Main extends App{
    val test = () => {
        println("test")
    }
    val test2 = (x:Int, y:Int) => x * y
    def sum(n: Int):Int = n match {
        case n if n < 2 => 1
        case n if n > 0 => n + sum(n - 1)
    }
    // val trg = (n:Int) => n match {
    //     case n if n > 0 => 1
    //     case _ => 0
    // }
    println("start")
    println(sum(5))
    println("end")
}
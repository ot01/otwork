const out = (obj) => {
    console.log(obj)
}
const fnc01 = () => {
    out('fnc01')
}

const list = [1,2,3]
out(list)

out('start')
list.forEach((i) => {
    out(i)
})
out('end')


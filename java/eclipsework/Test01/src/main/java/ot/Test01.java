package ot;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Test01 {
	public static void output(Object obj) {
		System.out.println(obj);
	}

//	java8 参考
//	https://eikatou.net/blog/20171015.html

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ
		test03();
	}

	// Java8のテスト
	private static void test03() {
		Path path = Paths.get("test.txt");
		// ファイルの読み込み（Close処理が必要！）
		try (Stream<String> fileLines = Files.lines(path)) {
			fileLines.forEach(System.out::println);
		}catch(Exception e) {
			output(e);
		}
	}

	private static void test02() {
		File file = new File("test.txt");
		try(
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
		){
			String line = "";
			while((line = br.readLine()) != null) {
				output(line);
			}
		}catch(Exception e) {
			output(e);
		}
	}

	private static void test01() {
		File file = new File("test.txt");

		try (FileOutputStream fos = new FileOutputStream(file)){
			fos.write("テスト文字列".getBytes());
		}catch(Exception e) {
			output(e);
		}
	}
}

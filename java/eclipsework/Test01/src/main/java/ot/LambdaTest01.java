package ot;

import java.util.Arrays;
import java.util.List;

public class LambdaTest01 extends Utils{

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ
		test03();
	}

	private static void test03() {
		List<String> list = Arrays.asList("tset","test2");
		list.stream()
			.forEach(x -> output(x));

		List<Integer> list2 = Arrays.asList(1,2,3,4);

		Integer sum = list2.stream()
					.mapToInt(x -> x)
					.sum();

		output(sum);
	}

	private static void test02() {
		List<String> list = Arrays.asList("tset","test2");
		list.stream()
			.forEach(x -> output(x));
	}

	private static void test01() {
		output("test01 start");

		SampleA s = new SampleA();

		SampleI i = x -> 100 + x;
		SampleI i2 = it -> it * 2;

		output(i.getVal(50));
		output(i2.getVal(200));

		SampleI i3 = new SampleI(){
			public int getVal(int x) {
				return x * 3;
			}
		};

		output(i3.getVal(300));

		output("test02 end");

	}

}

class SampleA{
	private int no = 0;

	public int getNo() {
		return no;
	}
}

interface SampleI{
	public int getVal(int x);
}
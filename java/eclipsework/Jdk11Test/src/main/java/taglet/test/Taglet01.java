package taglet.test;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.lang.model.element.Element;

import com.sun.source.doctree.DocTree;

//import com.sun.tools.doclets.Taglet;
import jdk.javadoc.doclet.Taglet;

public class Taglet01 implements Taglet{
//	public static boolean start(RootDoc root) {
//		return true;
//	}
	private static final String NAME = "ot.version";
	private static final String HEADER = "太田テスト";

	private static void output(Object obj) {
		System.out.println(obj);
	}
	private void output2(Object obj) {
		System.out.println(obj);
	}

	// oota 不要？
//	public static void register(final Map<String, Taglet> tagletMap) {
//		output("start");
//		Taglet01 taglet01 = new Taglet01();
//
//		Optional<Taglet> tag = Optional.ofNullable(tagletMap.get(NAME));
//		output(tag);
//		tag.ifPresent(it -> tagletMap.remove(NAME));
//
//		output("put pre");
//		tagletMap.put(NAME,  taglet01);
//		output("end");
//	}

	@Override
	public String getName() {
//		output2("1");
		// TODO 自動生成されたメソッド・スタブ
		return NAME;
	}

//	@Override
//	public boolean inConstructor() {
//		output2("2");
//		// TODO 自動生成されたメソッド・スタブ
//		return true;
//	}
//
//	@Override
//	public boolean inField() {
//		output2("3");
//		// TODO 自動生成されたメソッド・スタブ
//		return true;
//	}
//
//	@Override
//	public boolean inMethod() {
//		output2("3");
//		// TODO 自動生成されたメソッド・スタブ
//		return true;
//	}
//
//	@Override
//	public boolean inOverview() {
//		output2("4");
//		// TODO 自動生成されたメソッド・スタブ
//		return true;
//	}
//
//	@Override
//	public boolean inPackage() {
//		output2("5");
//		// TODO 自動生成されたメソッド・スタブ
//		return true;
//	}
//
//	@Override
//	public boolean inType() {
//		output2("6");
//		// TODO 自動生成されたメソッド・スタブ
//		return true;
//	}
//
//	@Override
//	public boolean isInlineTag() {
//		output2("6");
//		// TODO 自動生成されたメソッド・スタブ
//		return false;
//	}
//
//	@Override
//	public String toString(Tag arg0) {
//		output2("toString start");
//		// TODO 自動生成されたメソッド・スタブ
//		return arg0.text();
//	}
//
//	@Override
//	public String toString(Tag[] arg0) {
//		output2("toString[] start");
//		// TODO 自動生成されたメソッド・スタブ
//		return arg0[0].text();
//	}

	@Override
	public Set<Location> getAllowedLocations() {
//		output("1");
		Set<Location> set = new HashSet<Location>();
		Collections.addAll(set, Location.FIELD
				,Location.MODULE
				,Location.METHOD
				,Location.OVERVIEW
				,Location.PACKAGE
				,Location.TYPE);
		// TODO 自動生成されたメソッド・スタブ
		return set;
	}
	@Override
	public String toString(List<? extends DocTree> tags, Element element) {
//		output("2");
		output(tags);
		tags.stream().forEach(i -> output(i));
		output(element);
		// TODO 自動生成されたメソッド・スタブ
		return "arrayreturn";
	}
	@Override
	public boolean isInlineTag() {
//		output("3");
		// TODO 自動生成されたメソッド・スタブ
		return false;
	}

}

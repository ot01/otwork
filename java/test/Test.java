
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class Test{
    public static void main(String[] args){
        List<String> list = new ArrayList<String>();
        List<Integer> list2 = Arrays.asList(1,2,3,4);

        output(list2);

        list2.stream()
            .forEach(it -> output(it));
    }

    public static void output(Object obj){
        System.out.println(obj);
    }
}
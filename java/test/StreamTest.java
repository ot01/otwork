package test;

import ot.common.*;
import java.util.*;

/** stream */
/** http://d.hatena.ne.jp/nowokay/20130504 */
class StreamTest extends Basic{
	public static void main(String[] args){
		output("start");
		
		List<String> names = Arrays.asList("hoge","foo","test");
		
		test01();
		test02(names);
		test03(names);
		
		output("end");
	}
	
	private static void test03(List<String> names){
		names.stream().forEach(name -> output(name));
	}
	
	private static void test02(List<String> names){
		for(String s: names){
			output(s);
		}
	}
	
	private static void test01(){
		List<Integer> list = Arrays.asList(1,2,3);
		output(list);
		
		list.stream()
		.map(x -> x * 2)
			.forEach(x -> output(x));
	}
}